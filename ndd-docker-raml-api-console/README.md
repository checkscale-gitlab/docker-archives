# NDD Docker RAML API Console

A Docker image for the [RAML API Console](https://github.com/mulesoft/api-console).

## Installation

### From source

```shell
git clone git@bitbucket.org:ndd-docker/ndd-docker-raml-api-console.git
cd ndd-docker-raml-api-console
docker build -t ddidier/raml-api-console .
```

### From Docker Hub

```shell
docker pull ddidier/raml-api-console
```



## Usage

The API directory on the host `<HOST_DATA_DIR>` must be mounted as a volume under `/data` in the container.

Use `-v <HOST_DATA_DIR>:/data` to use a specific documentation directory or `-v $(pwd):/data` to use the current directory as the API directory.

Then you must create an HTML file in `<HOST_DATA_DIR>` referencing the parser and your RAML file, for example `<HOST_DATA_DIR>/some-api.html`:

```html
<!doctype html>

<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>API Console</title>
  <link href="../styles/api-console-light-theme.css" rel="stylesheet" class="theme">
</head>

<body ng-app="ramlConsoleApp" ng-cloak>
  <script src="../scripts/api-console-vendor.js"></script>
  <script src="../scripts/api-console.js"></script>
  <script>
    $.noConflict();
  </script>
  <raml-console-loader src="some-api.raml"
    options="{ disableThemeSwitcher: true, resourcesCollapsed: false}">
  </raml-console-loader>
</body>

</html>
```

Note that:

- the RAML file must be in the mounted directory or below
- the [available options](https://github.com/mulesoft/api-console#configuration) must be passed as JSON

Then run a container:

```shell
docker run -it --rm -p 9000:9000 -p 35729:35729 -v <HOST_DATA_DIR>:/data --name api-console ddidier/raml-api-console
```

And open your browser at `http://localhost:9000/apis/some-api.html`
