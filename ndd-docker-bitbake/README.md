# NDD Docker BitBake

https://www.yoctoproject.org/docs/1.6/bitbake-user-manual/bitbake-user-manual.html.

## Usage

```bash
docker run -it -v <HOST_DATA_DIR>:/data -e USER_ID=$UID ddidier/bitbake bash
# e.g.
docker run -it -v $(pwd)/tests:/data -e USER_ID=$UID ddidier/bitbake bash
```

## Tutorial

Distribution in `tests` is following the tutorial found at https://a4z.bitbucket.io/docs/BitBake/guide.html.

Found issues:

- Add `BB_SIGNATURE_HANDLER ?= "basichash"` to `bitbake.conf`.
