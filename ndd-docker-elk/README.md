# NDD Docker ELK

A Docker image for the [ELK stack](https://www.elastic.co/products).

The image is based upon the [sebp/elk image](https://hub.docker.com/r/sebp/elk/) and adds:

- some ElasticSearch plugins: Marvel and Sense
- some LogStash plugins: `logstash-filter-translate`
- some utilities: `gawk`, `jq`, `wget`


## Installation

### From source

```
git clone git@bitbucket.org:ndd-docker/ndd-docker-elk.git
cd ndd-docker-elk
docker build -t ddidier/elk .
```

### From Docker Hub

```
docker pull ddidier/elk
```


## Usage

An extensive [documentation of the base image](http://elk-docker.readthedocs.io/) is available.

Create a volume to store your data, for example:

```shell
docker volume create --name elk-data
```

Run the container, for example:

- with the data stored in a volume named `elk-data`
- with the LogStash configuration stored on the host in the directory `$logstash_dir`
- with the logs to ingest stored on the host in the directory `$logs_dir`

```shell
docker run -p 5601:5601 -p 9200:9200 -p 5044:5044 -p 5000:5000  \
           -v elk-data:/var/lib/elasticsearch           \
           -v $(logs_dir):/var/tmp/logs    \
           -v $(logstash_dir):/etc/logstash    \
           --name ddidier-elk ddidier/elk
```

[Register then install](https://www.elastic.co/guide/en/marvel/current/license-management.html) **YOUR** Marvel licence:

```shell
curl -XPUT -u admin 'http://localhost:9200/_license?acknowledge=true' -d @licence.json
```

You can log into it:

```shell
docker exec -it ddidier-elk bash
```

Stop it:

```shell
docker stop ddidier-elk
```
