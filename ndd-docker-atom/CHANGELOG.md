# NDD Docker Atom

## Version 0.2.0

- Update Atom version to `1.16.0`
- Fix entrypoint

## Version 0.1.0

- Initial commit
